# ReactChartjsAPIS3

## ReactChartjsAPIS3

## How to build from scratch

- [ ] [https://github.com/facebook/create-react-app ]
- [ ] [From Github, run ]

```
npm init react-app project_name
```

After done create,

```
cd project_name
npm test

npm start
```

To Build & Deploy

```
npm run build
```

## References

https://medium.com/codex/data-visualization-with-react-js-and-chart-js-cb6ca5d77ff6

https://stackoverflow.com/questions/60433572/how-to-use-axios-api-with-chart-js-and-react-js

S3 static
https://andela.com/insights/how-to-deploy-your-react-app-to-aws-s3/

ReactJS: Download CSV File on Button Click
https://stackoverflow.com/questions/53504924/reactjs-download-csv-file-on-button-click
